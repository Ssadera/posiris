<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  
    Route::get('signin','LoginController@index');
    Route::post('signin','LoginController@signin');
    Route::get('signout','LoginController@signout');
    Route::post('createUser','LoginController@createUser');
    //Password Reset
    Route::get('forgotpassword','LoginController@getforgotpassword');
    Route::post('forgotpassword','LoginController@postforgotpassword');
    //New user reset
    Route::post('newUser','RegistrationController@postnewpassword'); 
    Route::post('restore','UsersController@restore');

    Route::group(['prefix' => 'data'], function(){
    });
    Route::get('getterminaldetails','TransactionstatusController@getTerminalDetails');
    Route::group(['prefix' => 'admin'], function() {
        //user routes
        Route::resource('user', 'UsersController');   
        Route::get('getusers', ['as' => 'admin.getUsers', 'uses' =>'UsersController@index']);
        Route::post('editusers/{$id}', ['as' => 'admin.getUsers', 'uses'=> 'UsersController@edit']);
        Route::delete('delete/user', ['as' => 'admin.getUser','uses' => 'UsersController@delete']);
        Route::get('addusers', ['as' => 'admin.addUsers', 'uses' => 'UsersController@create']);
        //Deactivated users
        Route::get('lockedUsers','UsersController@lockedUsers');
        //New users list
        Route::get('approve','UsersController@approveUsers');
        //Approve user
        Route::post('check/{$id}','UsersController@userChecked');
        //terminal routes
        Route::get('getterminals', ['as' => 'admin.getterminals', 'uses' => 'TerminalController@index']);
        Route::get('addterminal', ['as' => 'admin.addterminal', 'uses' => 'TerminalController@create']);
        Route::post('editterminals/{$id}', ['as' => 'admin.editterminals', 'uses' => 'TerminalController@edit']);
        Route::delete('deleteterminal/{$id}', ['as' => 'admin.deleteterminal', 'uses' => 'TerminalController@destroy']);
        //software issues
        Route::get('gettransactionerrors', ['as' => 'admin.gettransactionerrors', 'uses' => 'TransactionErrorController@index']);
        Route::get('addterminal', ['as' => 'admin.addterminal', 'uses' => 'TerminalController@create']);
        Route::post('editterminals/{$id}', ['as' => 'admin.editterminals', 'uses' => 'TerminalController@edit']);
        Route::delete('deleteterminal/{$id}', ['as' => 'admin.deleteterminal', 'uses' => 'TerminalController@destroy']);

        Route::get('getTerminals', 'TabController@getTerminals');
        Route::get('getTransactions', 'TabController@getTransactions');
        Route::get('getchart', array('as' => 'admin.getchart', 'uses' => 'ChartController@index'));
        Route::get('userChart', array('as' => 'admin.userChart', 'uses' => 'ChartController@userChart'));

        Route::get('gethardwareerrors', ['as' => 'admin.gethardwareerrors', 'uses' => 'HardwareIssuesController@index']);
        Route::get('getoperatingsystem', ['as' => 'admin.gethardwareerrors', 'uses' => 'terminal_osController@index']);
        Route::get('addterminal', ['as' => 'admin.addterminal', 'uses' => 'TerminalController@create']);
        Route::post('editterminals/{$id}', ['as' => 'admin.editterminals', 'uses' => 'TerminalController@edit']);
        Route::delete('deleteterminal/{$id}', ['as' => 'admin.deleteterminal', 'uses' => 'TerminalController@destroy']);

        Route::get('gettransactions', ['as' => 'admin.gettransactions', 'uses' => 'TransactionsController@index']);
        Route::get('getterminaldetails/{$id}',['as' => 'admin.getterminaldetails', 'uses' =>'TransactionstatusController@getTerminalDetails']);
        Route::get('addterminal', ['as' => 'admin.addterminal', 'uses' => 'TerminalController@create']);
        Route::post('editterminals/{$id}', ['as' => 'admin.editterminals', 'uses' => 'TerminalController@edit']);
        Route::delete('deleteterminal/{$id}', ['as' => 'admin.deleteterminal', 'uses' => 'TerminalController@destroy']);

        Route::get('gettransactionstatus', ['as' => 'admin.gettransactionstatus', 'uses' => 'transactionstatusController@index']);
        Route::post('getloc', ['as' => 'admin.transactionstatus', 'uses' => 'TransactionstatusController@showTestResults']);

        Route::post('getid', ['as' => 'admin.checkUsers', 'uses' => 'UsersController@checkUsers']);

        Route::get('addusers', ['as' => 'admin.addUsers', 'uses' => 'UsersController@create']);
        Route::post('addusers', ['as' => 'admin.addUsers', 'uses' => 'UsersController@store']);
        Route::get('edituser/{$id}', ['as' => 'admin.editUser/{$id}', 'uses' => 'UsersController@show']); 
        //Editing Profile picture
        Route::get('profile', ['as' => 'admin.Profile', 'uses' => 'ProfileController@editProfile']);     
        Route::post('editProfile', ['as' => 'admin.editProfile', 'uses' => 'ProfileController@update_avatar']);
        //Editing Profile Details 
        Route::post('profile/edit', ['as' => 'profile.edit', 'uses' => 'ProfileController@updateProfile']);   

        Route::get('roles', ['as' => 'admin.roles', 'uses' => 'UsersController@roles']);  

        Route::get('register/verify/{confirmationCode}', ['as' => 'confirmation_path', 'uses' => 'RegistrationController@confirm' ]);
        Route::post('register/verify/{confirmationCode}', ['as' => 'confirmation_path', 'uses' => 'RegistrationController@postconfirm' ]);
});    
Route::get('home/?id={$id}', 'HomeController@index');
Route::get('chartjs', 'HomeController@chartjs');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
