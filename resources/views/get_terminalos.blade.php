@extends('admin_template')

@section('content')
   <!-- Main content -->
  <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Terminals</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class = "table table-bordered" id="myTable">
                <thead>                
                  <th>#</th>
                  <th>OBJ</th>
                   <th>Terminal Id</th>
                  <th>Operating System</th>
                  <th>Date</th>                          
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($osversion as $terminal)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$terminal['obj']}}</td>
                    <td>{{$terminal['terminalid']}}</td>
                    <td>{{$terminal['os']}}</td>
                    <td>{{$terminal['tdate']}}</td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="Http://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#myTable').DataTable();
    });
    </script>
@endsection