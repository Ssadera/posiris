@extends('admin_template')

@section('content')
   <!-- Main content -->
   <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          @include('layouts.message-block')
            <div class="box-header">
              <h3 class="box-title">Locked Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="lockedUsers" class="table table-bordered table-hover" text-alignment="center">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User Role</th>
                      <th>Email</th>
                      <th>Username</th>
                      <th>Status</th>
                      <th>UnLock</th>                        
                    </tr>
                  </thead>
              <tbody>
              <?php $counter = 1;?>
              @foreach($users as $user)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$user['role']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['username']}}</td>
                    <td><b>Locked</b></td>
                    <td>
                    <a href="#unlockModal" class="btn btn-warning" data-toggle="modal">UnLock</a>
                    <!--unLock Modal-->
                      <div id="unlockModal" action="{{URL::to('restore/'.$user['id'])}}" class="modal fade unlock-modal" tabindex="-1" role="dialog" aria-labelledby="unlockModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><b>UnLock</b></h4>
                        </div>
                        <div class="modal-body">
                        <p>Are you sure you want to unlock <b>{{ $user['username'] }}</b>? </p>
                        </div>
                        <div class="modal-footer">
                        <form action="{{URL::to('restore/'.$user['id'])}}" id="restore-form{{ $user['id'] }}" method="post" class="hidden">
                         <!-- <input name="_method" type="hidden" value="PUT"> -->
                        <div class="form-group">
                        <label for="username" class="col-sm-6 control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" name="username" value="{{ $user['username'] }}" class="form-control" >
                        </div>
                        </div></br></br>
                        <div class="form-group">
                        <label for="Email" class="col-sm-6 control-label">Email</label>
                        <div class="col-sm-7">
                            <input type="email" name="email" value="{{ $user['email'] }}" class="form-control" >
                        </div>
                        </div></br></br>
                        <div class="form-group">
                        <label for="Password" class="col-sm-6 control-label" >Password</label>
                        <div class="col-sm-7">
                           <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        </div></br></br>
                        </br></br>
                        <div class="form-group">
                        <div class="checkbox-inline">
                        <label>
                            <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="1">System Administrator</label> 
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="2">Bank Administrator</label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="3">Merchant | Agent </label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="4">Field Agent</label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                          <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="5">Support</label>   
                        </div>
                        </div>
                        </form>
<!--                     <a href="#" class="btn btn-success" data-dismiss="modal">NO</a>
                         <a href="{{URL::to('restore/'.$user['id'])}}" class="btn btn-success">YES</a> -->
                          {!! Form::open(['method' => 'POST', 'url' => 'restore']) !!}
                          {!! Form::hidden('user_id', $user['id']) !!}
                          {!! Form::button('NO', ['class' => 'btn btn-default', 'data-dismiss' =>'modal']) !!}
                          {!! Form::submit('YES', ['class' => 'btn btn-success']) !!}
                          {!! Form::close() !!} 
                        </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div>
                    <script>
                    $(document).on('click','.unlock-btn{{ $user['id'] }}', function(event){
                      event.preventDefault();
                      $('.unlock-modal{{ $user['id'] }}').modal();
                    });
                    </script>
                    </td>
                  </tr>
                 @endforeach
               </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="Http://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#lockedUsers').DataTable();
    });
    </script>
@endsection