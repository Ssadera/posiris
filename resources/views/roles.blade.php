@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Roles Key:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class = "table table-bordered" id="myTable">
                <thead>              
                  <th>Role Id</th>
                   <th>Role Name</th>
                  <th>Role Description</th>                          
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($roles as $role)
                  <tr>
                    <td><b>{{$role['roleId']}}</b></td>
                    <td>{{$role['roleName']}}</td>
                    <td>{{$role['description']}}</td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection