<a href="javascript:;" class="btn btn-danger delete-btn{{ $id }}">Delete</a>

@php $username = empty($username) ? 'Test' : $name; @endphp

<div class="modal fade delete-modal {{ $id }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete {{ $username }} ? </p>
      </div>
      <div class="modal-footer">
      <form action="{{ url()->current().'/'.$id }}" id="delete-form{{ $id }}" method="post" class="hidden">
      <input type="hidden" name="_method" value="DELETE">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">


      </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" form="delete-form{{ $id }}" id="submit{{ $id }}" value="Delete" class="btn btn-danger">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).on('click', '.delete-btn{{ $id }}', function(event){
		event.preventDefault();
		$('.delete-modal{{ $id }}').modal();
	});
</script>