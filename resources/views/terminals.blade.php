@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
    
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          @include('layouts.message-block')
            <div class="box-header">
              <h3 class="box-title">Terminal  (id of the terminal and the name of the user and location) Details:</h3>
            </div>
            <hr>
            <!-- /.box-header -->
              <div id="exTab1" class="container"> 
                <ul  class="nav nav-pills">
                    <li class="active">
                      <a  href="#terminal_details" data-toggle="tab"><b>Terminal Details</b></a>
                    </li>
                    <li><a href="#agent_details" data-toggle="tab"><b>Agent/Merchant Details</b></a>
                    </li>
                    <li><a href="#software_details" data-toggle="tab"><b>POS Software Details</b></a>
                    </li>
                    <li><a href="#hardware_details" data-toggle="tab"><b>Hardware Details</b></a>
                    </li>
                    <li><a href="#card_errors" data-toggle="tab"><b>Card Errors</b></a>
                    </li>
                    <li><a href="#transaction_errors" data-toggle="tab"><b>Transaction Errors</b></a>
                    </li>
                  </ul>
                  <div class="tab-content clearfix">
                    <div class="tab-pane active" id="terminal_details">
                    <table>
                      <tr>
                        <th></th>
                        <th></th>
                      </tr>
                      <tr>
                        <td><span><b>POS ID</b></span></td>
                        <td><span>POSO4895</span></td>
                      </tr>
                      <tr>
                        <td><span><b>CURRENCY</b></span></td>
                        <td><span>RWF</span></td>
                      </tr>
                      <tr>
                        <td><span><b>TERMINAL SERIAL NUMBER</b></span></td>
                        <td><span>123456432121</span></td>
                      </tr>
                      <tr>
                        <td><span><b>POS LOCATION</b></span></td>
                        <td><span>KIGALI</span></td>
                      </tr>
                    </table>
                    </div>
                    <div class="tab-pane" id="agent_details">
                      <table>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td><span><b>MERCHANT AGENT NAME</b></span></td>
                          <td><span>BANK OF KIGALI</span></td>
                        </tr>
                        <tr>
                          <td><span><b>PHYSICAL ADDRESS</b></span></td>
                          <td><span>KIGALI</span></td>
                        </tr>
                        <tr>
                          <td><span><b>MERCHANT/AGENT TELEPHONE NUMBER</b></span></td>
                          <td><span>0743898322</span></td>
                        </tr>
                        <tr>
                          <td><span><b>BK SUPPORT NUMBER</b></span></td>
                          <td><span>0746272722</span></td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane" id="software_details">
                      <table>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td><span><b>LAST ACTIVITY</b></span></td>
                          <td><span>31/12/2016</span></td>
                        </tr>
                        <tr>
                          <td><span><b>APPLICATIONS IN POS</b></span></td>
                          <td><span>8445920961.AGN|36280331.AGN|30650430.AGN</span></td>
                        </tr>
                        <tr>
                          <td><span><b>SDK VERSION</b></span></td>
                          <td><span>84477401</span></td>
                        </tr>
                        <tr>
                          <td><span><b>TELIUM MANAGER VERSION</b></span></td>
                          <td><span>37772802</span></td>
                        </tr>
                        <tr>
                          <td><span><b>APPLICATION VERSION</b></span></td>
                          <td><span>M3.9</span></td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane" id="hardware_details">
                      <table>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td><span><b>TERMINAL CHARGING</b></span></td>
                          <td><span>Charging</span></td>
                        </tr>
                        <tr>
                          <td><span><b>BATTERY PERCENTAGE</b></span></td>
                          <td><span>94</span></td>
                        </tr>
                        <tr>
                          <td><span><b>NETWORK BARS</b></span></td>
                          <td><span>5</span></td>
                        </tr>
                        <tr>
                          <td><span><b>TEMPARATURE IN DEGRESS</b></span></td>
                          <td><span>25</span></td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane" id="card_errors">
                      <table>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td><span><b>1</b></span></td>
                          <td><span>Card Processing Error AT 2015-09-13 13:16:40</span></td>
                        </tr>
                        <tr>
                          <td><span><b>2</b></span></td>
                          <td><span>Card Processing Error AT 2016-04-13 13:16:40</span></td>
                        </tr>
                        <tr>
                          <td><span><b>3</b></span></td>
                          <td><span>Card Processing Error AT 2015-08-13 13:16:40</span></td>
                        </tr>
                        <tr>
                          <td><span><b>4</b></span></td>
                          <td><span>Card Processing Error AT 2015-08-13 13:16:40</span></td>
                        </tr>
                        <tr>
                          <td><span><b>5</b></span></td>
                          <td><span>Card Processing Error AT 2015-08-13 13:16:40</span></td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane" id="transaction_errors">
                      <table>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td><span><b>52</b></span></td>
                          <td><span>No check account</span></td>
                        </tr>
                        <tr>
                          <td><span><b>54</b></span></td>
                          <td><span>Expired card</span></td>
                        </tr>
                        <tr>
                          <td><span><b>55</b></span></td>
                          <td><span>Incorrect PIN</span></td>
                        </tr>
                        <tr>
                          <td><span><b>59</b></span></td>
                          <td><span>Suspected fraud</span></td>
                        </tr>
                      </table>

                    </div>
                  </div>
              </div>
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection