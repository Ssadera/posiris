@extends('admin_template')


<!-- app/views/nerds/index.blade.php -->

...

    @foreach($Users as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->role_id }}</td>
            <td>{{ $value->username }}</td>
            <td>{{ $value->password }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY
/nerds/{id} -->
                <!-- we will add this later since its a little more
complicated than the other two buttons -->
                {{ Form::open(array('url' => 'user/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this User', array('class'=> 'btn btn-danger')) }}
                {{ Form::close() }}

                <!-- show the nerd (uses the show method found at GET
/nerds/{id} -->
                <!-- <a class="btn btn-small btn-success" href="{{
URL::to('nerds/' . $value->id) }}">Show this Nerd</a> -->

                <!-- edit this nerd (uses the edit method found at GET
/nerds/{id}/edit -->
                <!-- <a class="btn btn-small btn-info" href="{{
URL::to('nerds/' . $value->id . '/edit') }}">Edit this Nerd</a> -->

            </td>
        </tr>
    @endforeach