@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Role</th>
                  <th>Email</th>
                  <th>Username</th>
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($users as $user)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$user['Role']}}</td>
                    <td>{{$user['Email']}}</td>
                    <td>{{$user['Username']}}</td>
                  </tr>
                 @endforeach
        
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection