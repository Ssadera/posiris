@extends('admin_template')
@section('content')
   <!-- Main content -->
  <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
           @include('layouts.message-block')
            <div class="box-header">
              <h3 class="box-title">New Users:</h3>
            </div>
            <div class="box-body">
              <input type="hidden" name="maker" value="{{Auth::User()->username}}" class="form-control">
              <table id="users" class="table table-bordered table-hover" text-align= "center">
              <thead>
                  <tr>
                    <th>#</th>
                    <th>User Role</th>
                    <th>Full Name</th>
                    <th>Mobile No</th>
                    <th>Email</th>
                    <th>Approve</th>               
                  </tr>
              </thead>
              <tbody>
              <?php $counter = 1;?>
              @foreach($users as $user)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$user['role']}}</td>
                    <td>{{$user['fullName']}}</td>
                    <td>{{$user['mobile']}}</td>
                    <td>{{$user['email']}}</td>
                    <form class="form-inline"  action="{{URL::to('admin/getid/')}}" method="POST">
                    {{ Form::hidden('invisible', $user['id']) }}
                    <td><input type="submit" value="Approve" class="btn btn-success"></td>
                    </form>
                  </tr>
                 @endforeach
               </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="Http://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#users').DataTable();
    });
    </script>
@endsection