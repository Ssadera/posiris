<!DOCTYPE html>
    <!--
    This is a starter template page. Use this page to start your new project from
    scratch. This page gets rid of all links and provides the needed markup only.
    -->
    <html>
    <head>
        <meta charset="UTF-8">
        <title>POSIris  | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Style.css  -->
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css" />
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ URL::asset('bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ URL::asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link href="{{ URL::asset('bower_components/admin-lte/dist/css/skins/skin-blue.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- data tables -->
        <link href="{{ URL::asset('bower_components/admin-lte/plugins/datatables/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

      <!-- Morris chart -->
      <link rel="stylesheet" href="{{ URL::asset('bower_components/admin-lte/plugins/morris/morris.css') }}">   
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        {!! Charts::assets() !!}

    </head>
    <body class="skin-blue">
       <div class="wrapper">

        <!-- Main Header -->
        @include('header')
        <!-- Sidebar -->
        @include('sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
<!--             <section class="content-header">
                <h1>
                    {{ $page_title or "Page Title" }}
                    <small>{{ $page_description or null }}</small>
                </h1>
            </section> -->

            <!-- Main content -->
            <section class="content">
              <!-- Info boxes -->
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                   <span class="info-box-icon bg-aqua"><a href="http://localhost:1234/posiris/public/admin/gettransactionstatus"><i class="fa fa-tablet"></i></a></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Terminals</span>
                      <span class="info-box-number" id="terminal"></span>
                    </div>
                    <!-- /.info-box-content --> 
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-red"><a href="http://localhost:1234/posiris/public/admin/gettransactions"><i class="fa fa-exchange"></i></a></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Transaction Data</span>
                      <span class="info-box-number" id="transactiondata"></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
<!--                 <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-download"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Software</span>
                      <span class="info-box-number">760</span>
                    </div>
                  </div>
                </div> -->
                <!-- /.col -->
<!--                 <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-cogs"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Hardware</span>
                      <span class="info-box-number">2,000</span>
                    </div>
                  </div>
                </div> -->
                <!-- /.col -->
              </div>
              <!-- /.row -->

                <!-- Your Page Content Here -->
                @yield('content')

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Main Footer -->
        @include('footer')

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery 2.1.3 -->
    <script src="{{ URL::asset('bower_components/admin-lte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ URL::asset ('bower_components/admin-lte/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::asset ('bower_components/admin-lte/dist/js/app.min.js') }}" type="text/javascript"></script>
     <!-- app js -->
    <script src="{{ URL::asset ('js/app.js') }}" type="text/javascript"></script>
    <!-- plugins -->
    <!-- datatable -->
     <script src="{{ URL::asset ('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
     <!-- pie chart -->
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

     <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{ URL::asset ('bower_components/admin-lte/plugins/morris/morris.min.js') }}"></script>
    <!-- Sparkline -->

     <!-- datatable -->
     <script src="{{ URL::asset ('bower_components/admin-lte/dist/js/pages/dashboard.js') }}" type="text/javascript"></script>

      <!-- datatable -->
     <script src="{{ URL::asset ('bower_components/admin-lte/dist/js/pages/dashboard2.js') }}" type="text/javascript"></script>
          <!-- datatable -->
     <script src="{{ URL::asset ('bower_components/admin-lte/dist/js/app.js') }}" type="text/javascript"></script>

      <!-- datatable -->
     <script src="{{ URL::asset ('bower_components/admin-lte/dist/js/demo.js') }}" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience -->
    </body>
</html>
