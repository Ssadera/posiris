<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Your payment partner.
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2016 <a href="http://tracom.co.ke/">Tracom Services Limited</a>.</strong> All rights reserved.
</footer>