<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            body{
                background-color: #b5bcc9;
               }
            h4 {
                text-align: center;
                text-decoration: underline;
                text-decoration-color: #444444;
                }
            #content{
                text-align: center;
               }

            #verif{
                background-color: #3c8dbc;
                height: 2.5em;
                font-family: inherit;
            }
            .anchor{
                color: #FFF;
                text-decoration: none;
            }
        </style>     
    </head>
    <body>
        <h4><b>POS</b>iris New User Verification EMail.</h4>
        <div id= "content">
            You are receiving this email because you have been added to our posiris system.<br><br>Click the below button to activate your account<br><br>
             <button id="verif"><a href="{{ URL::to('admin/register/verify/' . $confirmation_code) }}" class="anchor" ><b>VERIFY YOUR REGISTRATION EMAIL</b></a></button>        
        </div>
    </body>
</html>