@extends('layouts.login')

@section('content')
  <div class="login-box">
    <div class="login-logo">

      <a href="{{URL::to('/signin')}}"><figure><a href="#"><img src={{('img/posiris.png')}} alt="Logo"></a></figure></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg"><b>Sign in</b></p>
      <form action="{{URL::to('signin')}}" method="post">
        @include('layouts.message-block')  
        <div class="form-group has-feedback">
          <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username" autofocus>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password"> 
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <a href="{{URL::to('forgotpassword')}}">I forgot my password</a><br>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <input type="submit" value='Sign In' class="btn btn-primary btn-block btn-flat">
          </div>
          <!-- /.col -->
        </div>
      </form>
    <!--<a href="{{URL::to('createUser')}}" class="text-center">Register a new membership</a>-->
    </div>
    <!-- /.login-box-body -->
    </div>
@endsection
