@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit User: {{ $user['username'] }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if (Session::has('message'))
             <div class="alert alert-info">{{ Session::get('message') }}</div>
             @endif
            <!-- form  -->
            <form class="form-inline" action="{{URL::to('admin/user/'.$user['id'].'/edit')}}"  method ="POST">
            <input name="_method" type="hidden" value="PUT">
             <div class="form-group">
            <label for="username" class="col-sm-6 control-label">Username</label>
            <div class="col-sm-7">
                <input type="text" name="username" value="{{ $user['username'] }}" class="form-control"  required="">
            </div>
            </div></br></br>
            <div class="form-group">
            <label for="Email" class="col-sm-6 control-label">Email</label>
            <div class="col-sm-7">
                <input type="email" name="email" value="{{ $user['email'] }}" class="form-control"  required="">
            </div>
            </div></br></br>
            <div class="form-group">
                   <label for="Password" class="col-sm-6 control-label" >Password</label>
             <div class="col-sm-7">
                <input type="password" name="password" class="form-control" placeholder="Password">
             </div>
             </div></br></br>
             <div class="form-group">
             <div class="checkbox-inline">
                  <label>
                      <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="{{ $user['role_id'] }}">Support Level 1
                  </label>
              </div></br></br>
              <div class="checkbox-inline">
                  <label>
                       <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="{{ $user['role_id'] }}">Support Level 2
                  </label>
              </div></br></br>
              <div class="checkbox-inline">
                  <label>
                       <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="{{ $user['role_id'] }}">Support Level 3
                  </label>
                </div></br></br>
                <div class="checkbox-inline">
                    <label>
                         <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="{{ $user['role_id'] }}">Support Level 4
                    </label>
               </div>
             </div></br></br>
              <div class="form-group">
                <input type="submit" value="Update User" class="btn btn-primary">
              </div>
                
            </form>
            <!-- form  -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
