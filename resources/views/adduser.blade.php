@extends('admin_template')
@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          @include('layouts.message-block')
            <div class="box-header">
              <h3 class="box-title">Create User Form:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!-- form  -->
            <form class="form-inline"  action="{{URL::to('admin/addusers/')}}" method="POST">
                <div class="form-group has-feedback">
                   <label for="fullName" class="col-sm-6 control-label">Enter Full Names:</label>
                   <div class="col-sm-7">
                       <span class="form-control-feedback"></span>
                       <input type="text" name="fullName" class="form-control" placeholder="eg- John Doe">
                   </div>
                </div></br></br>
                <input type="hidden" name="maker" value="{{Auth::User()->username}}" class="form-control">
                <div class="form-group has-feedback">
                   <label for="mobile" class="col-sm-6 control-label">Mobile Number:</label>
                   <div class="col-sm-7">
                       <span class="form-control-feedback"></span>
                       <input type="text" name="mobile" class="form-control" placeholder="eg- +254710123456">
                   </div>
                </div></br></br>
                <div class="form-group has-feedback">
                   <label for="username" class="col-sm-6 control-label">Enter Username:</label>
                   <div class="col-sm-7">
                       <span class="form-control-feedback"></span>
                       <input type="text" name="username" class="form-control" placeholder="eg- jdoe123">
                   </div>
                </div></br></br>
                <div class="form-group has-feedback">
                     <label for="Email" class="col-sm-6 control-label">Enter Email Address:</label>
                 <div class="col-sm-7">
                     <span class="form-control-feedback"></span>
                     <input type="email" name="email" class="form-control"  placeholder="eg- jdoe@example.com">
                </div>
                 </div></br></br>
                 <div class="form-group has-feedback ">
                        <label for="Password" class="col-sm-6 control-label" >Temporary Password:</label>
                  <div class="col-sm-7">
                    <span class="form-control-feedback"></span>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  </div></br></br>
                  <div class="form-group has-feedback">
                  <label for="roles" class="col-sm-6 control-label" >User Role:</label></br></br>
                  <span></span>
                 <div class="checkbox-inline">
                   <label>
                       <input type="checkbox" class="md1-checkbox_input" name="role_id" value="1">System Administrator
                   </label>
                 </div><span></span>
                  <div class="checkbox-inline">
                   <label>
                        <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="2">Bank Administrator
                   </label>
                </div><span></span>
                <div class="checkbox-inline">
                   <label>
                        <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="3">Merchant | Agent
                   </label>
                </div><span></span>
                <div class="checkbox-inline">
                   <label>
                        <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="4">Field Agent
                   </label>
                 </div><span></span>
                  <div class="checkbox-inline">
                     <label>
                          <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="5">Support
                     </label>
                   </div>
                  </div></br></br>
                  <div class="form-group">
                  <input type="submit" name="add"  class="btn btn-default">
                  </div>
            </form>
            <!-- form  -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection