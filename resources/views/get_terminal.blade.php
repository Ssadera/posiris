@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Terminals</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="terminals_tables" class="table table-striped table-bordered">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Terminal Id</th>
                  <th>Merchant</th>
                  <th>Location</th>
                  <th>Serial No</th>
                  <th>Phone</th>                 
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($terminals as $terminal)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$terminal['Terminal_id']}}</td>
                    <td>{{$terminal['Merchant']}}</td>
                    <td>{{$terminal['Location']}}</td>
                    <td>{{$terminal['Serial_number']}}</td>
                    <td>{{$terminal['Phone']}}</td>
                  </tr>
                 @endforeach
                 </tbody>  
                 <tfoot>
                 </tfoot>     
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection