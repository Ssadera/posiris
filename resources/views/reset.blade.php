@extends('layouts.reset')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><b>POSiris Password Reset</b></div>
                <div class="panel-body">
                    @if( Session::has('status') )
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <p>{{ Session::get('status') }}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                    @include('layouts.message-block')
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('password/email') }}">
                    {{ csrf_field() }}                   
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
