@extends('admin_template')

<style type="text/css">
  
  / The ribbons /

.corner-ribbon{
  width: 200px;
  background: #e43;
  position: absolute;
  top: 25px;
  left: -50px;
  text-align: center;
  line-height: 50px;
  letter-spacing: 1px;
  color: #f0f0f0;
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}

/ Custom styles /

.corner-ribbon.sticky{
  position: fixed;
}

.corner-ribbon.shadow{
  box-shadow: 0 0 3px rgba(0,0,0,.3);
}

/ Different positions /
.corner-ribbon.bottom-right{
  top: auto;
  right: -50px;
  bottom: 25px;
  left: auto;
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}

/ Colors /
.corner-ribbon.white{background: #f0f0f0; color: #555;}
.corner-ribbon.black{background: #333;}
.corner-ribbon.grey{background: #999;}
.corner-ribbon.blue{background: #39d;}
.corner-ribbon.green{background: #2c7;}
.corner-ribbon.turquoise{background: #1b9;}
.corner-ribbon.purple{background: #95b;}
.corner-ribbon.red{background: #e43;}
.corner-ribbon.orange{background: #e82;}
.corner-ribbon.yellow{background: #ec0;}
</style>

@section('content')
   <!-- Main content -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <section class="content">   
      <?php $counter = 1;?>
     <form class="form-inline"  action="{{URL::to('admin/getloc/')}}" method="POST">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 wow fadeInLeft" data-wow-delay="0.1s">
                    <div class="thumb-pad3">
                        <div class="thumbnail">                          
                            <div class="caption">                               
                                {{ Form::select('loc', $loc, null, array('id' => 'locid', 'name'=>'locname')) }}                                                          
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 wow fadeInLeft" data-wow-delay="0.1s">
                    <div class="thumb-pad3">
                     <div class="thumbnail">                       
                      <div class="form-group">
                      <input type="submit" value="Search" class="btn btn-primary"> 
                      </div>
                      </div>
                    </div>
                </div>
      </div>
      </form>
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Terminal Status</h3>
            </div>
            
            <!-- /.box-header -->

           

                <?php //echo "<pre>"; print_r($tran);
                ?>

                 @foreach($tran as $t)
                 <?php //echo "<pre>"; print_r($t);
                ?>

                 <div class="col-lg-1 col-md-1 col-sm-6 col-xs-6 wow fadeInLeft" data-wow-delay="0.1s">
                    <div class="thumb-pad3">
                        <div class="thumbnail">
                           
                            <div class="caption">
             
                                <figure><a href="{{ url('getterminaldetails')}}"><img src={{asset('img/ingict220.jpg')}} alt="Logo"></a>
                                  </figure>
                               
                                
                                <p>{{$t['TERMINAL_ID']}}</p>
                               <!--  <h6>{{$t['TERMINAL_EXID']}}</h6> -->
                                <?php
                                  $date = $t['OSYSDATE'];
                                  $date1 = \Carbon\Carbon::now()->subday()->toDateString();

                                  if($date >= $date1)
                                  {
                                    ?>
                                    <div class="corner-ribbon bottom-right sticky green shadow">Active</div>
                                    <?php
                                  }
                                  else
                                  {
                                    ?>

                                    <div class="corner-ribbon bottom-right sticky red shadow">Inactive</div>
                                    <?php
                                   
                                  }
                                ?>
                                
                                
                                <!-- <div class="corner-ribbon bottom-right sticky orange shadow">Active</div> -->
                            </div>  
                        </div>
                    </div>
                </div>               
                 @endforeach
            <div class="box-body">
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <script>

$(document).ready(function()
{   
  // function to get all records from table
  function getAll(){
    
    $.ajax
    ({
      url: url+'/posiris/public/admin/getTerminals',
      type:'GET',
      cache: false,
      success: function(r)
      {
        $("#display").html(r);
      }
    });     
  }
  
  getAll();
  // function to get all records from table
  
  
  // code to get all records from table via select box
  $("#getProducts").change(function()
  {       
    var id = $(this).find(":selected").val();

    var dataString = 'action='+ id;
        
    $.ajax
    ({
      url: 'getproducts.php',
      data: dataString,
      cache: false,
      success: function(r)
      {
        $("#display").html(r);
      } 
    });
  })
  // code to get all records from table via select box
});

      $(function(){ 
        'use strict';  
        var url = window.location.origin;



        $('#locid').change(function(){
        var selectedVal = $('#locid option:selected').text(); //this is how you get the selected value with jQuery
        $.ajax({

            type: "POST",
            url: url+'/posiris/public/admin/select',
            data: {
                id: selectedVal
            },
            dataType: 'json', // force the ajax request to return json data
            success: function(returnedData){
              console.log(returnedData)
                 // ...
            }
        });
      });
      });
    </script>
     
      <!-- /.row -->
    </section>

    <!-- /.content -->
@endsection