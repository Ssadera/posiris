<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar Menu -->
     <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-tachometer" aria-hidden="true"></i>MENU</li>
        <!-- Optionally, you can add icons to the links -->
        @if(Auth::user()->role_id == 1)
            <li><a href="{{URL::to('admin/getchart')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
            <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Users</span></a>
            <ul class="treeview-menu">
        <!-- <li><a href="{{URL::to('admin/userChart/')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Users' Chart</span></a></li> -->
                <li><a href="{{URL::to('admin/getusers/')}}"><i class="fa fa-users" aria-hidden="true"></i><span>View Active Users</span></a></li>
                <li><a href="{{URL::to('admin/lockedUsers')}}"><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i><span>View Locked Users</span></a></li>
                <li><a href=""><i class="fa fa-user-plus" aria-hidden="true"></i><span>Create New User</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/roles')}}"><i class="glyphicon glyphicon-tasks"></i><span>Roles Key</span></a></li>
                    <li><a href="{{URL::to('admin/addusers/')}}"><i class="fa fa-user-plus"></i><span>Create User</span></a></li>  
                </ul>
                </li>
                <li><a href="{{URL::to('admin/approve/')}}"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i><span>Approve Users</span></a></li>
            </ul>
            </li> 
<!--             <li class="treeview">
                <a href="{{URL::to('admin/network')}}"><i class="fa fa-signal"></i><span>Network</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/network/mtn')}}"><i class="fa fa-signal"></i><span>MTN</span></a></li>
                    <li><a href="{{URL::to('admin/network/vivacell')}}"><i class="fa fa-signal"></i><span>Vivacell</span></a></li>
                    <li><a href="{{URL::to('admin/network/safaricom')}}"><i class="fa fa-signal"></i><span>Safaricom</span></a></li>
                </ul>
            </li>  -->
            <li><a href="{{URL::to('admin/getterminals')}}"><i class="glyphicon glyphicon-alert" aria-hidden="true"></i><span>Terminal Errors</span></a>
            <ul class="treeview-menu">
            <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-move" aria-hidden="true"></i><span>Hardware Errors</span></a>
            <ul class="treeview-menu">
                <li><a href="{{URL::to('admin/gethardwareerrors')}}"><i class="glyphicon glyphicon-compressed" aria-hidden="true"></i><span>Hardware</span></a>
            </ul>
                <li><a href="{{URL::to('admin/#')}}"><i class="glyphicon glyphicon-erase" aria-hidden="true"></i><span>Software</span></a>
            <ul class="treeview-menu">
            <li><a href="{{URL::to('admin/getoperatingsystem')}}"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i><span>Operating System</span></a>
            </ul>
            </ul>
            </li>
            <li><a href="{{URL::to('admin/gettransactionstatus')}}"><i class="fa fa-tablet" aria-hidden="true"></i><span>Terminal</span></a></li>
            <li><a href="{{URL::to('admin/gettransactions')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Transaction</span></a>
            <ul class="treeview-menu">
               <li><a href="{{URL::to('admin/gettransactions')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Successfull</span></a>
               <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i><span>Failed</span></a>
                <ul class="treeview-menu">
                <li><a href="{{URL::to('admin/gethardwareerrors')}}"><i class="glyphicon glyphicon-gift" aria-hidden="true"></i><span>Card Errors</span></a>
                <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></i><span>Failed Transactions</span></a>
                </ul>
            </ul>
            </li>
            <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="fa fa-download" aria-hidden="true"></i><span>Sofware Issues</span></a></li>
            @elseif(Auth::user()->role_id == 2)
            <li><a href="{{URL::to('admin/getchart')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
            <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Users</span></a>
            <ul class="treeview-menu">
                <li><a href="{{URL::to('admin/getusers/')}}"><i class="fa fa-users" aria-hidden="true"></i><span>View Active Users</span></a></li>
                <li><a href="{{URL::to('admin/lockedUsers')}}"><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i><span>View Locked Users</span></a></li>
                <li><a href="{{URL::to('admin/addusers/')}}"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i><span>Create New User</span></a></li>
            </ul>
            </li>
<!--             <li class="treeview">
                <a href="{{URL::to('admin/network')}}"><i class="fa fa-signal"></i><span>Network</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/network/mtn')}}">MTN</a></li>
                    <li><a href="{{URL::to('admin/network/vivacell')}}">Vivacell</a></li>
                </ul>
            </li>  -->
            <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-tablet" aria-hidden="true"></i><span>Terminals</span></a>
            <ul class="treeview-menu">
            <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-move" aria-hidden="true"></i><span>Hardware</span></a>
            <ul class="treeview-menu">
                <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-compressed" aria-hidden="true"></i><span>Hardware</span></a>
            </ul>
                <li><a href="{{URL::to('admin/#')}}"><i class="glyphicon glyphicon-erase" aria-hidden="true"></i><span>Software</span></a>
            <ul class="treeview-menu">
            <li><a href="{{URL::to('admin/getoperatingsystem')}}"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i><span>Operating System</span></a>
            </ul>
            </ul>
            </li>
            <li><a href="{{URL::to('admin/transactions')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Transaction Totals</span></a>
            <ul class="treeview-menu">
               <li><a href="{{URL::to('admin/gettransactions')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Successfull</span></a>
               <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i><span>Failed</span></a>
               <ul class="treeview-menu">
                <li><a href="{{URL::to('admin/gethardwareerrors')}}"><i class="glyphicon glyphicon-gift" aria-hidden="true"></i><span>Card Errors</span></a>
                <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></i><span>Failed Transactions</span></a>
               </ul>
            </ul>
            </li>
            <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="fa fa-download" aria-hidden="true"></i><span>Sofware Issues</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-exclamation-triangle"></i><span>Hardware Errors!</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/software')}}">Battery</a></li>
                    <li><a href="{{URL::to('admin/software')}}">Network</a></li>
                </ul>
            </li>
            @elseif(Auth::user()->role_id ==3)
<!--             <li class="treeview">
                <a href="{{URL::to('admin/network')}}"><i class="fa fa-signal"></i><span>Network</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/network/mtn')}}">MTN</a></li>
                    <li><a href="{{URL::to('admin/network/vivacell')}}">Vivacell</a></li>
                </ul>
            </li>  -->
            <li><a href="{{URL::to('admin/terminals')}}"><i class="fa fa-tablet" aria-hidden="true"></i><span>Terminals</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-move" aria-hidden="true"></i><span>Hardware</span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-compressed" aria-hidden="true"></i><span>Hardware</span></a>
                    </ul>
                        <li><a href="{{URL::to('admin/#')}}"><i class="glyphicon glyphicon-erase" aria-hidden="true"></i><span>Software</span></a>
                    <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/getoperatingsystem')}}"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i><span>Operating System</span></a>
                    </ul>
                </ul>
            </li>
            <li><a href="{{URL::to('admin/transactions')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Transaction</span></a>
                <ul class="treeview-menu">
                       <li><a href="{{URL::to('admin/gettransactions')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Successfull</span></a>
                       <li><a href="{{URL::to('admin/users')}}"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i><span>Failed</span></a>
                       <ul class="treeview-menu">
                       <li><a href="{{URL::to('admin/gethardwareerrors')}}"><i class="glyphicon glyphicon-gift" aria-hidden="true"></i><span>Card Errors</span></a>
                       <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></i><span>Failed Transactions</span></a>
                       </ul>
                </ul>
            </li>
            <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="fa fa-download" aria-hidden="true"></i><span>Sofware Issues</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-exclamation-triangle"></i><span>Hardware Errors!</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/software')}}">Battery</a></li>
                    <li><a href="{{URL::to('admin/software')}}">Network</a></li>
                </ul>
            </li>
           @elseif(Auth::user()->role_id == 4)
               <li><a href="{{URL::to('admin/gettransactionerrors')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
               <li><a href="{{URL::to('admin/software')}}"><i class="fa fa-download" aria-hidden="true"></i><span>Sofware Issues</span></a></li>
               <li class="treeview">
                <a href="#"><i class="fa fa-exclamation-triangle"></i><span>Hardware Errors!</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/software')}}">Battery</a></li>
                    <li><a href="{{URL::to('admin/software')}}">Network</a></li>
                </ul>
            </li>         
        @else
        @endif
    </ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>

