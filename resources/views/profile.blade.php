@extends('admin_template')
@section('content')
   <!-- Main content -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
          {{$user['fullName']}}'s Profile
          </h4>
          <ol class="breadcrumb">
            <li><a href="{{URL::to('admin/getchart')}}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active"><a href="#">Profile</a></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">      
              <!-- Profile Image -->
              <div class="box box-success">
                <div class="box-body box-profile">
                  <img src="../uploads/avatars/{{ $user->avatar }}" class="profile-user-img img-responsive img-circle" alt="Profile Pic">
                  <form enctype="multipart/form-data" action="editProfile" method="POST">
                      <label>Update Profile Image</label>
                      <input type="file" name="avatar">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"></br>
                      <input type="submit" class="btn btn-sm btn-success">
                  </form>
                  <p class="text-muted text-center">{{$user['role']}}</p>
                  <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                      <b>Full names</b><a class="pull-right">{{$user['fullName']}}</a>
                    </li>
                   <li class="list-group-item">
                      <b>Username</b><a class="pull-right">{{$user['username']}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Email</b><a class="pull-right">{{$user['email']}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Mobile No</b><a class="pull-right">{{$user['mobile_no']}}</a>
                    </li>
                  </ul>
                  <!-- <a href="#editProfile" data-toggle="tab" class="btn btn-success btn-block">Edit Profile</a> -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
<!--             <div class="col-md-9">
                  <div class="box box-success" >
                    <div class="box-header with-border">
                       <h3 class="box-title"><i class="fa fa-pencil-square"></i><span>Edit Form</span></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                </div>
                 @include('layouts.message-block')

                 </div> -->
        </section><!-- /.content -->
    <!-- /.content -->
@endsection
