@extends('admin_template')

@section('content')
   <!-- Main content -->
   <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Transactions</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Terminal Id</th>                
                  <th>Status</th>              
                  <th>Date</th>
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($transactions as $transaction)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$transaction['Terminal']}}</td>
                    <td>{{$transaction['Message']}}</td>            
                     <td>{{$transaction['Date']}}</td>                   
                  </tr>
                 @endforeach
        
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="Http://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#users').DataTable();
    });
    </script>
@endsection