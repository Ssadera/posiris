@extends('admin_template')
@section('content')
   <!-- Main content -->
  <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.css"> 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
           @include('layouts.message-block')
            <div class="box-header">
              <h3 class="box-title">View Users:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="users" class="table table-bordered table-hover" text-align= "center">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User Role</th>
                      <th>FullName</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Mobile No</th>
                      <th>Status</th>
                      <th>Edit</th>
                      <th>Lock</th>
                      <th>Delete</th>                
                    </tr>
                  </thead>
              <tbody>
              <?php $counter = 1;?>
              @foreach($users as $user)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$user['role']}}</td>
                    <td>{{$user['fullName']}}</td>
                    <td>{{$user['username']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['mobile']}}</td>
                    <td><b>Active</b></td>
                    <td>  
                    <a href="#editModal" class="btn btn-success" data-toggle="modal">Edit</a>
                    <!-- Edit User modal -->
                    <div id="editModal" class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><b>Edit User:</b>{{ $user['username'] }}</h4>
                        </div>
                        <div class="modal-body">
                        <form action="{{URL::to('admin/user/'.$user['id'])}}" id="edit-form{{ $user['id'] }}" method="post" >
                        <input name="_method" type="hidden" value="PUT">
                        <div class="form-group">
                        <label for="username" class="col-sm-6 control-label">Username</label>
                        <div class="col-sm-7">
                           <input type="text" name="username" value="{{ $user['username'] }}" class="form-control" >
                        </div>
                        </div></br></br>
                        </br><div class="form-group">
                        <label for="Email" class="col-sm-6 control-label">Email</label>
                        <div class="col-sm-7">
                           <input type="email" name="email" value="{{ $user['email'] }}" class="form-control" >
                        </div>
                        </div></br></br>
                        <div class="form-group">
                        <label for="Password" class="col-sm-6 control-label" >Password</label>
                        <div class="col-sm-7">
                           <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        </div></br></br>
                        </br></br>
                        <div class="form-group">
                        <div class="checkbox-inline">
                        <label>
                            <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="1">System Administrator</label> 
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="2">Bank Administrator</label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="3">Merchant | Agent </label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                             <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="4">Field Agent</label>
                        </div></br></br>
                        <div class="checkbox-inline">
                        <label>
                          <input type="checkbox" class="md1-checkbox_input"  name="role_id" value="5">Support</label>   
                        </div>
                        </div>
                        </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <input type="submit" form="edit-form{{ $user['id']}}" id="submit{{ $user['id']}}" value="Update" class="btn btn-success">
                        </div>
                        </div><!-- /.modal-content -->
                      </div>
                      <!-- End modal-dialog -->
                    </div>
                    <script>
                      $(document).on('click','.edit-btn{{ $user['id'] }}', function(event){
                        event.preventDefault();
                        $('.edit-modal{{ $user['id'] }}').modal();
                      });
                    </script>
                    </td>
                    <td>
                    <a href="#lockModal" class="btn btn-warning" data-toggle="modal">Lock</a>
                    <!--Lock Modal-->
                      <div id="lockModal"  class="modal fade delete-modal" tabindex="-1" role="dialog" aria-labelledby="lockModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><b>Lock</b></h4>
                        </div>
                        <div class="modal-body">
                        <p>Are you sure you want to lock <b>{{ $user['username'] }}? </b></p>
                        </div>
                        <div class="modal-footer">
                        <form action="{{url::to('admin/user/'.$user['id'])}}" id="deactivate-form{{ $user['id'] }}" method="post" class="hidden">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                          <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                          <input type="submit" form="deactivate-form{{ $user['id']}}" id="submit{{ $user['id']}}" value="YES" class="btn btn-warning">
                        </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div>
                    <script>
                    $(document).on('click','.lock-btn{{ $user['id'] }}', function(event){
                      event.preventDefault();
                      $('.lock-modal{{ $user['id'] }}').modal();
                    });
                    </script>
                    </td>
                    <td>
                    <a href="#deleteModal" class="btn btn-danger" data-toggle="modal" >Delete</a>
                    <!-- delete modal -->
                    <div id="deleteModal" class="modal fade delete-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><b>Delete</b></h4>
                        </div>
                        <div class="modal-body">
                        <p>Are you sure you want to delete<b> <u>{{ $user['username'] }}</u></b>  permanently from system ? </p>
                        </div>
                        <div class="modal-footer">
                        <form action="{{URL::to('admin/delete/user')}}" id="delete-form{{ $user['id'] }}"  method="post" class="hidden">
                        <input name="id" type="hidden" value="{{ $user['id'] }}">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token">
                        </form>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" form="delete-form{{ $user['id']}}" id="submit{{ $user['id']}}" value="Delete" class="btn btn-danger">
                        </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <script>
                    $(document).on('click','.delete-btn{{ $user['id'] }}', function(event){
                      event.preventDefault();
                      $('.delete-Modal{{ $user['id'] }}').modal();
                    });
                  </script>
                    </td>
                  </tr>
                 @endforeach
               </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="Http://cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function(){
    $('#users').DataTable();
    });
    </script>
@endsection