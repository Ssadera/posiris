@extends('admin_template')

@section('content')
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Transaction Errors</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Transaction Date</th>
                  <th>ERROR_CODE</th>
                  <th>Error Description</th>
                  <th></th>   
                  <th>PIFD</th>
                  <th>TERMINAL_ID</th>                 
                </tr>
                </thead>
                <tbody>
                <?php $counter = 1;?>
                 @foreach($transactionerrors as $transactionerror)
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td>{{$transactionerror['t_Date']}}</td>
                    <td>{{$transactionerror['errorcode']}}</td>
                    <td>{{$transactionerror['errorcode']}}</td>
                    <td>{{$transactionerror['pfid']}}</td>
                    <td>{{$transactionerror['Terminal_id']}}</td>
                  </tr>
                 @endforeach
        
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection