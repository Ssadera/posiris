@extends('layouts.reset')

@section('content')

<?php echo $confirmation_code;?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">POSiris New User Password Reset</div>
                <div class="panel-body">
                @if ($confirmation_code)
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('newUser') }}">
                        {{ csrf_field() }}
                        @include('layouts.message-block')
                        <input type="hidden" name="confirmation_code" value="{{ $confirmation_code }}">

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" >
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                @elseif($confirmation_code = null)
                    <h1>{{ $message }}</h1>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
