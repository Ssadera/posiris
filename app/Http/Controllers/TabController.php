<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Terminal;

use App\Transactions;

use Response;

use View;

use DB;

class TabController extends Controller
{
    public function getTerminals()
    {
     $terminal = Terminal::count();
     
     return Response::json(array('message'=>'success','data'=>$terminal));

    }
     public function getTransactions()
    {
        $transactiondata = Transactions::where('OSYSDATE', '<', 'CURRENT_DATE + INTERVAL 1 DAY')->count();
        
        return Response::json(array('message'=>'success','data'=>$transactiondata));

    }
}