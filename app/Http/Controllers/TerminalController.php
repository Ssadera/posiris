<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

use App\Terminal;
use App\Merchant;
use App\Location;


class TerminalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terminals = Terminal::all();
        foreach ($terminals  as $key => $value) {
            $terminal['Terminal_id']= $value['TERMINAL_ID'];
            $terminal['Terminal_name']= $value['TERMINAL_NAME'];
            $merchant = Merchant::where('MERCHANT_ID','=',$value['MERCHANT_ID'])->first();
            $terminal['Merchant'] = $merchant->MERCHANTNAME;
            $location = Location::where('LOCATION_ID','=',$value['LOCATION_ID'])->first();
            $terminal['Location']=$location->LOCATION_NAME;
            $terminal['Serial_number']=$value['SIMSERIALNO'];
            $terminal['Phone']=$value['PHONE_NO'];
            
            $appdata[] = $terminal;
        }
        $data['terminals'] = $appdata;

        return view('get_terminal')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
