<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use App\Role;
use Image;
use Redirect;

class ProfileController extends Controller
{
   public function editProfile()
   {
    return view('profile',array('user'=> Auth::user()));
   }
   public function edit($id)
    {
    $user = User::find($id);
    $user = json_decode(json_encode($user), true);
    return redirect::to('profile')
                    ->with('user', $user); 
    }
   public function updateProfile(Request $request)
    {
    $data = $request->all();
    // validate
    $rules = array(
                'fullName' =>'required',
                'username' => 'required',
                'mobile' =>'required'            
    );
    $validator = Validator::make($data , $rules);
        if ($validator->fails()) {
                return Redirect::to('admin/profile')
                        ->withErrors($validator)
                        ->with($data);
           }else{
            //update user
            $message = "Profile Update Successful";
            $user = User::where('id','=',$data['id'])->first();
            $user->fullName   = $data['fullName'];
            $user->username   = $data['username'];
            $user->mobile_no  = $data['mobile'];
            $user->save();
            //redirect
            return back()->with(['message'=> $message]);
          }
    }
   public function update_avatar(Request $request)
   {
   	//Handle the user upload of avatar
   	if($request->hasFile('avatar')){
   		$avatar= $request->file('avatar');
   		$filename = time() .'.'. $avatar->getClientOriginalExtension();
   		Image::make($avatar)->resize(300,300)->save(public_path('/uploads/avatars/'. $filename));
   		$user =Auth::user();
   		$user->avatar= $filename;
   		$user->save();
   	}
   	return view('profile',array('user'=> Auth::user()));
   }
}
