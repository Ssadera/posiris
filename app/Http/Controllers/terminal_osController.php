<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\terminal_os;

class terminal_osController extends Controller
{
	    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $osversion = terminal_os::all();
    foreach ($osversion as $key => $value) {	
    	$terminal['tmp']= $value['TMP'];
        $terminal['obj']= $value['OBJ'];
        $terminal['os']= $value['OS'];
        $terminal['tdate']= $value['TDATE'];
        $terminal['terminalid']= $value['TERMINAL_ID'];
        $terminal['Terminal_name']= $value['TERMINAL_NAME'];
        $appdata[] = $terminal;
    }
     $data['osversion'] = $appdata;

     return view('get_terminalos')->with($data);
	}
}
