<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use Illuminate\Support\MessageBag;
use Validator;
use Redirect;
use Hash;
use Auth;
//models
use App\User;
class LoginController extends Controller
{
   /**
   * Show the form for creating a new resource.
   *
   * @return login page
   */
  public function index()
  {
      return view('/login');
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function signin(Request $request)
  {
      $data = $request->all();
      $rules = array(
          'username' => 'required', 
          'password' => 'required',
      );
      $validator = Validator::make($data, $rules);
      if ($validator->fails()) {
            return Redirect::to('signin')
                            ->withInput($request->only(['username']))
                            ->withErrors($validator);                     
      }else{
      $usr = $data['username'];
      $usermaker = User::where('username','=',$usr)->value('checker');     
      if($usermaker == 'NULL')
       {
        // $message = "Your account account is not activated";
        return Redirect::to('signin')
                        ->with('status', 'wrong')
                        ->with('message','Your account account is not activated!');
       }
       elseif ($usermaker != 'NULL'){
          if (Auth::attempt($data))
          { 
          return Redirect('admin/getchart');
          }else{
            $message = "These credentials do not match our records.";
              return Redirect::to('signin')
                   ->with($request->only('username'))
                   ->with(['message' => $message]);
            }
         }
       }
    }
  //signout
  public function signout()
  {
      $message = "You Have Logged out!";
      Auth::logout();
      return Redirect::to('/signin')->with(['message' => $message]);
  }
  //creating a a user
  public function createUser(Request $request)
  {
      $data = $request->all();
      if ($data) {
          $user = new User;
          $user->username = $data['username'];
          $user->email = $data['email'];
          $user->password = Hash::make($data['password']);
          $user->role_id = $data['role'];
          if($user->save()){
               return "Okay";
          }
          return "Not Okay";           
      }
  }
  public function getforgotpassword($token = null)
  {
  return view('reset')
                  ->with('token', $token);
  }
  public function postforgotpassword(Request $request)
  {
     $this->validate($request, [
                       'token' => 'required',
                       'email' => 'required|email',
                       'password' => 'required|confirmed',
                       'password_confirmation' => 'required'
                       ]);
     $credentials = $request->only(
         'email', 'password', 'password_confirmation', 'token'  
     );
     $response = Password::reset($credentials, function ($user, $confirmed) {
         $this->resetPassword($user, $password);
     }); 
     switch ($response) {
         case Password::PASSWORD_RESET:
             return redirect($this->redirectPath())
                                  ->with('Message','Password Reset Successfull!');
         default:
             return redirect()->back()
                         ->withInput($request->only('email'))
                         ->withErrors(['email' => trans($response)]);
    }
  }
}
