<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Support\MessageBag;
use Validator;
use Redirect;
use Hash;
use Auth;
//models
use App\User;
use Password;

class RegistrationController extends Controller 
{
  public function confirm($confirmation_code)
  {
      if($confirmation_code){
              $user = User::whereConfirmationCode($confirmation_code)->first();
          if ($user){
             $message = "Account Activation Successfull.Reset Password to Login.";
            return view('newUser')->with('confirmation_code', $confirmation_code)
                                  ->with(['message' => $message]);
          }
      }
  }
  public function postnewpassword(Request $request)
  {
      $data = $request->all();
      if ($data["password"] == $data["password_confirmation"]){
            $user = User::where('confirmation_code','=',$data['confirmation_code'])->first();
            $user->password=Hash::make($data['password']); 
            $user->confirmation_code=Null;
            $user->confirmed= 1;
            $welcome = "Password Reset Successfull.";
            if($user->save())
                return Redirect::to('/signin')->with(['message'=>$welcome]);
      }else if ($data["password"] != $data["password_confirmation"]) {
        return view('newUser')->withErrors($data); 
      }else{
        return view('newUser');
      }
  }
}