<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

use App\Terminal;
use App\TransactionError;
use App\MessageMap;


class TransactionErrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $transactionerrors = TransactionError::all(); 
        foreach ($transactionerrors  as $key => $value) {
            $description = MessageMap::where('ERROR_CODE','=',$value['ERROR_CODE'])->first();
            $transactionerror['description'] = $description->ERROR_MESSAGE;
            $transactionerror['t_Date']= $value['T_DATE'];
            $transactionerror['Terminal_id']= $value['TERMINAL_ID'];
            $transactionerror['pfid']= $value['PIFD'];
            $transactionerror['errorcode']= $value['ERROR_CODE'];   
                         
            
            $appdata[] = $transactionerror;
        }
        $data['transactionerrors'] = $appdata;
        return view('get_transaction_errors')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
