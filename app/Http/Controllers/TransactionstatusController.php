<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transactions;

use App\Terminal;

use App\Location;

use DB;

use Illuminate\Support\Facades\Input;

use Response;

class TransactionstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $tran = DB::select("CALL get_transaction_details(1)");  
         // echo "<pre>";
         // print_r($tran);
         // die();

        // $tran = DB::select('CALL get_transaction_details(@)'); 
        
         $transactions = json_decode(json_encode($tran),true);
        // //$loc = Location::lists('LOCATION_ID', 'LOCATION_NAME')->prepend('');
        // // echo "<pre>";
        // // print_r($transactions);
        // // die(); 
         $locs = Location::pluck('LOCATION_NAME', 'LOCATION_ID');
           //   echo "<pre>";
           //  print_r($categories);
           // die(); 
                                   
         return view('trans_status')->with(['tran'=>$transactions])->with(['loc'=>$locs]);
             
        
        //return Response::json(array('message'=>'success','data'=>$terminal));
    	
    }
    public function getTerminalData()
    {
        $tran = DB::select('CALL get_transaction_details(@a)');

        $transactions = json_decode(json_encode($tran),true);

        return Response::json(array('message'=>'success','data'=>$transactions));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showTestResults(Request $request)
    {
        $questionId = Input::get('locname');
        $tran = DB::select("CALL get_transaction_details($questionId)"); 
        $transactions = json_decode(json_encode($tran),true);
        $locs = Location::pluck('LOCATION_NAME', 'LOCATION_ID');
      
        return view('trans_status')->with(['tran'=>$transactions])->with(['loc'=>$locs]);

    }

    public function getTerminalDetails()
    {
        return view('terminals');
    }


}
