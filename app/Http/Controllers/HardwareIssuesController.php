<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

use App\HardwareIssues;

class HardwareIssuesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $hardware_issues = HardwareIssues::all();
        
        foreach ($hardware_issues  as $key => $value) {
            // $ = Role::where('role_id','=',$value['role_id'])->first();
            $hardware_issue['pfid']= $value['PIFD'];
            $hardware_issue['tdate']= $value['TDATE'];
            $hardware_issue['terminal_id']= $value['TERMINAL_ID'];
            $hardware_issue['errorcode']= $value['ERROR_CODE'];
           
            
            $appdata[] = $hardware_issue;
        }
        $data['hardware_issues'] = $appdata;

        return view('get_hardware_errors')->with($data);
    }
}
