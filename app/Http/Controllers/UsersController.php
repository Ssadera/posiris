<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use Hash;
use Mail;
use App\User;
use App\Role;
use Image;
use View;
use Illuminate\Support\Facades\Input;

class UsersController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
    $appusers = User::where('checker','!=','NULL')->get();
    $appdata = [];
    foreach ($appusers  as $key => $value) {
        $role = Role::where('role_id','=',$value['role_id'])->first();
        $user['role'] = $role->role_name;
        $user['email']=$value['email'];
        $user['fullName']=$value['fullName'];
        $user['username']=$value['username'];
        $user['mobile']=$value['mobile_no'];
        $user['id']=$value['id'];  
        $appdata[] = $user;
        }
    $data['users'] = $appdata;
    return view('get_users')
              ->with($data);
  }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function create()
  {
        return view('adduser');
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
  public function store(Request $request)
  {
        $data = $request->all();
        $rules = array
                (
                'fullName' => 'required',
                'mobile' => 'required',
                'username' => 'required',
                'email'    => 'required|email|unique:users', 
                'password' => 'required',
                'role_id' => 'required',
                );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()){
                return Redirect::to('admin/addusers')->with($data)      
                                          ->withErrors($validator);
        }else{
            $user = new User;
            $confirmation_code = str_random(35);
            $user->fullName=$data['fullName'];
            $user->mobile_no=$data['mobile'];
            $user->username=$data['username'];
            $user->role_id=$data['role_id'];
            $user->email=$data['email']; 
            $user->maker= $data['maker']; 
            $user->checker='NULL';
            $user->password=Hash::make($data['password']);
            $user->confirmation_code=$confirmation_code;
            $conCode= array('confirmation_code' => $confirmation_code, );
            if($user->save())
            {         
            Mail::send('email.verify', $conCode, function($message) use ($data){
            $message->from('info@tracom.co.ke', 'Tracom');
            $message->to($data['email'],$data['username'])
                             ->subject('POSiris New User verification email');
            });
            return Redirect::to('admin/addusers')->with('message','New user created  successfully!');             
            }
            return Redirect::to('admin/addusers');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return View('admin.getusers')
                 ->with('user', $user);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $user = json_decode(json_encode($user), true);
        return redirect::to('editusers')
                        ->with('user', $user); 
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $data = $request->all();
    //validate
    $rules = array(
                'username' => 'required',
                'email'    => 'required|email',
                'role_id' => 'required',
                'password' => 'required'
    );
    $validator = Validator::make($data , $rules);
        if ($validator->fails()){
                return View('editusers')
                        ->withErrors($validator)
                        ->with($data);
            }else{
            //store
            $user = User::find($id);
            $user->username   = $data['username'];
            $user->email  = $data['email'];
            $user->role_id = $data['role_id'];
            $user->password =Hash::make($data['password']);
            $user->save();
            //redirect
            $message = "User Updated successfully";
            return back()->with(['message' => $message]);
            }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */       
    public function destroy($id)
    {    
        $message = "User Locked Successfully";  
        $user = User::find($id)->delete();
        return back()->with(['message' => $message]);
    }
    public function delete(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $user = User::find($id);
        $user = User::where('id',$id);
        $user->forceDelete();
        if($user){
            return back()->with('message','User Deleted permenantly from system');
        }else{
            return back()->with('message','Error in deleting user');
        }
    }
    public function approveUsers()
    { 
        $appusers = User::where('maker','!=', Auth::user()->username)->get(); 
        $appdata = [];
        foreach ($appusers  as $key => $value) {
            $role = Role::where('role_id','=',$value['role_id'])->first();
            $user['role'] = $role->role_name;
            $user['mobile']=$value['mobile_no'];
            $user['email']=$value['email'];
            $user['fullName']=$value['fullName'];
            $user['username']=$value['username']; 
            $user['id']=$value['id'];   
            $user['avatar']=$value['avatar'];
            $appdata[] = $user;
            }
            $data['users'] = $appdata; 
            if($data){
                return view('approve')->with($data);
            }elseif ($appdata->isEmpty()) {
              echo "There are no users to approve";
              
            }
    }
    public function checkUsers(Request $request)
    {
            $questionId = Input::get('invisible'); 
            $user = User::all()->where('id','=',$questionId)->first();
            $user->checker = Auth::user()->username;
            $user->maker = NULL;
            $user->save();
            $message = "User Approved successfully";
            return back()->with(['message' => $message]);
    }
    public function lockedUsers()
    {
    $appusers = User::onlyTrashed()->paginate(20);
    $appdata = [];
    foreach ($appusers  as $key => $value) {
        $role = Role::where('role_id','=',$value['role_id'])->first();
        $user['role'] = $role->role_name;
        $user['email']=$value['email'];
        $user['username']=$value['username'];
        $user['id']=$value['id'];   
        $user['avatar']=$value['avatar'];
        $appdata[] = $user;
        }
        $data['users'] = $appdata; 
        if($data){
            return view('lockedUsers')->with($data);
        }else{
            return back()->with('message','Error in deleting user');
        }     
    }
    public function restore(Request $request)
    {
      $message = "User Unlocked Successfully";
      $data = $request->all();
      $user =User::withTrashed()->find($data['user_id'])->restore();
      return back()->with(['message'=>$message]);
    }
    public function roles()
    {
      $task = Role::all();
      foreach ($task as $key => $value) {
        $role['roleId']=$value['role_id'];
        $role['roleName']=$value['role_name'];
        $role['description']=$value['description']; 
        $appdata[] = $role;
      }
    $data['roles'] = $appdata;
    return view('roles')
              ->with($data);
    }
}