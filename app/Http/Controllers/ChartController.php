<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transactions;

use App\MessageMap;

use App\ErrorCode;

use App\User;

use DB;

use Charts;

class ChartController extends Controller
{
public function index()
    { 
    //Transaction Data Pie Chart
    $chart = Charts::database(Transactions::all(),'pie', 'c3')
                ->responsive(true)
                ->title('Summary')
                ->dimensions(0, 0)
                ->groupBy('FIELD_39', true)
                ->elementLabel('Transactions')
                ->labels(['Refer to card issuer','Refer to card issuer, special condition','Approved or completed successfully','Invalid merchant','Do not honor'])
                ->credits(false);                                   
    //Signals Line Graph
    // $line = Charts::realtime(route('data'),1000, 'line', 'highcharts')
    //             ->responsive(true)
    //             ->title('Signal Activity')
    //             ->dimensions(1000,1000)
    //             ->elementLabel('Terminal Signal Data')
    //             ->credits(false); 
    //Users Bar chart
    $bar = Charts::multi('bar', 'highcharts')
                ->title('Activity')
                ->labels(["Active","Dormant", "Inactive"])
                // ->groupByMonth()
                ->Dataset('Active ',[600, 800, 550])
                ->Dataset('Dormant',[400, 350, 300])
                ->Dataset('Inactive',[50, 40, 30])
                ->elementLabel('Totals')
                ->responsive(false)
                ->width(0)
                ->height(400)              
                ->Colors(["rgb(44, 160, 44)","rgb(255, 127, 14)","rgb(214, 39, 40)"])     
                ->credits(false);

    return view('chart',['chart' => $chart,'bar' => $bar,]);
    }   
}







	
