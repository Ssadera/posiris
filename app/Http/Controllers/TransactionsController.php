<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;

use App\Transactions;

use App\MessageMap;

class TransactionsController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transactions::all();
        foreach ($transactions  as $key => $value) {
        	$transaction['Terminal']= $value['FIELD_41'];
        	$message  = MessageMap::where('ERROR_CODE','=',$value['FIELD_39'])->first();
        	$transaction['Message']= $message->ERROR_MESSAGE;
        	$transaction['Date']= $value['OSYSDATE'];
            $appdata[] = $transaction;
        }
        $data['transactions'] = $appdata;
        return view('get_transactions')->with($data);
    }
}
