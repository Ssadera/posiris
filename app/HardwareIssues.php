<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HardwareIssues extends Model
{
    protected $table = 'pos_iris_card_errors';
}
                           // [PIFD] => 1
                           //  [TDATE] => 2015-06-03 11:00:15
                           //  [TERMINAL_ID] => BKP00894
                           //  [ERROR_CODE] => Invalid track
                           //  [TUSER] => Parcelle 6112,
                           // [PIFD] => 2
                           //  [TDATE] => 2015-06-08 11:00:15
                           //  [TERMINAL_ID] => BKP00290
                           //  [ERROR_CODE] => Invalid track
                           //  [TUSER] => Parcelle 6112,
							// [PIFD] => 3
                           //  [TDATE] => 2015-06-10 11:00:15
                           //  [TERMINAL_ID] => BKP00265
                           //  [ERROR_CODE] => Invalid track
                           //  [TUSER] => Parcelle 6112,
							// [PIFD] => 4
                           //  [TDATE] => 2015-06-15 11:00:15
                           //  [TERMINAL_ID] => BKP00129
                           //  [ERROR_CODE] => Invalid track
                           //  [TUSER] => Parcelle 6112,
							// [PIFD] => 5
                           //  [TDATE] => 2015-06-25 11:00:15
                           //  [TERMINAL_ID] => BKP00304
                           //  [ERROR_CODE] => Invalid track
                           //  [TUSER] => Parcelle 6112,
