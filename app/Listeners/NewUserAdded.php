<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserAdded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Handle the event.
     *
     * @param  Added  $event
     * @return void
     */
    public function Create(Added $event)
    {
        // dd($event);
        $event->user->notify(new \App\Notifications\WelcomeEmail());
    }
}
