<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageMap extends Model
{
    protected $table = 'response_codes_mapping';
}
