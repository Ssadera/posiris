<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionError extends Model
{
    protected $table = 'pos_iris_failed_trans';
}
